<?php

/**------------------------------------------------------------------------------------------------------------------------------------------------
 * @@Name: Master_model
 
 * @@Author: Kyei Amos Mensah <'buyitgh@gmail.com'>
 
 * @Date:   			2018-08-22 11:54:30
 * @Last Modified by:   Kyei Amos Mensah
 * @Last Modified time: 2018-08-24 11:13:04

 * @Copyright: 			MobileContent.Com Ltd <'owner'>
 
 * @Website: 			https://mobilecontent.com.gh
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */


class Master_model extends CI_Model
{

	/**
	*	@@param construct
	*	@@return NULL {load db}
	*/
    function __construct() {
        
        parent::__construct(); 

        $this->load->database();
    }

    ## Guys lets begin writing our DB Model function Logic below

    //ussd extention creation................
    public function save_new_ussd_extention($client_id, $ussd_code, $date_stamp)
    {
        $new_ussd_request = "INSERT INTO mc_ussd(client_id,ussd_code,`created_date`) VALUES (
        '".$this->db->escape_str($client_id)."',
        '".$this->db->escape_str($ussd_code)."',
        '".$this->db->escape_str($date_stamp)."')"; // code

        $insert_records =$this->db->query($new_ussd_request);
        //check if record is inserted successfully...................
        if ($insert_records === TRUE){
            return $insert_records;
        }else
        {
            return FALSE;
        }
    }

    /**
    *	Insert Into Token table 
    */
    public function insert_token($token, $client_id, $date_stamp)
    {
    	// insert into token table
    	$insert_process = "INSERT INTO mc_token(token, client_id, `created_date`) VALUES (
         '".$this->db->escape_str($token)."',
         '".$this->db->escape_str($client_id)."',
         '".$this->db->escape_str($date_stamp)."')";
         // execute query
        $insert_records = $this->db->query($insert_process);
        if ($insert_records === TRUE) {
         	return $insert_records;
         }
         else{
         	return FALSE;
         } 
    }
    

    /**
     * Client token 🔑 generation method
     */
    public function generate_client_token()
    {
        $token_prefix = 'mc-pay-user-token-';
        $token_suffix = substr(md5(time()), 0, 12);
        $full_token = $token_prefix.$token_suffix;
        return $full_token;
    }

    //checking if the client token matches after a client made a request...............
    public function check_for_matching_client_token($client_token)
    {
        //get detatils and match it ...............
        $this->db->order_by('mc_token.token');
        $this->db->join('mc_token','mc_token.client_id = mc_ussd.client_id');
        $result = $this->db->where('token',$client_token);
        // var_dump($result);
        if ($result->num_rows() == 1) 
        {
            return TRUE;
        }else
        {
            return FALSE;
        }
    }


    public function verify_client_token($client_token, $client_ussd_code)
    {
        $this->db->select('t.token, s.ussd_code');
        $this->db->from('mc_token as t');
        $this->db->where('t.token', $client_token);
        $this->db->where('t.ussd_code',$client_ussd_code);
        $this->db->join('mc_ussd as s', 't.client_id = s.client_id', 'LEFT');
        $query = $this->db->get();
        // var_dump($query);
        if($query->num_rows != 0)
        {
            return TRUE; //$query->result();
        }
        else
        {
            return FALSE;
        }
    }
}