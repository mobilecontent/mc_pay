<?php

/**------------------------------------------------------------------------------------------------------------------------------------------------
 * @@Name: index
 
 * @@Author: Kyei Amos Mensah <'buyitgh@gmail.com'>
 
 * @Date:   			2018-08-22 11:54:30
 * @Last Modified by:   Kyei Amos Mensah
 * @Last Modified time: 2018-08-23 14:25:32

 * @Copyright: 			MobileContent.Com Ltd <'owner'>
 
 * @Website: 			https://mobilecontent.com.gh
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */

if (!defined('BASEPATH'))
    exit('What do you think you\'re *DOING* boy..?  be Careful not to get the Village Elders *ANGRY* !!!'); //prevent direct Access.!
// Prepare the Header.!
$this->load->view("templates/_parts/_dashboard/dashboard_header.php");
?>

<div class="container body">
  <div class="main_container">


    <!-- Begin Left side Menu control how side / left menu is displayed -->
    <?php
if ($this->ion_auth->user()->row()->user_type == 'admin') {
    $this->load->view("templates/_parts/_dashboard/admin/dashboard_left_sidebar_admin.php");
}

// Prepare the client sidebar if logged in as a User.
if ($this->ion_auth->user()->row()->user_type == 'user') {
    $this->load->view('templates/_parts/_dashboard/client/dashboard_left_sidebar_client.php');
}
?>
    <!-- End Left menu -->



    <!-- top navigation -->
    <?php
if ($this->ion_auth->user()->row()->user_type == 'admin') {
    $this->load->view("templates/_parts/_dashboard/admin/dashboard_topnav_admin.php");
}

// Prepare the client sidebar if logged in as a User.
if ($this->ion_auth->user()->row()->user_type == 'user') {
    $this->load->view('templates/_parts/_dashboard/client/dashboard_topnav_client.php');
}

?>
    <!-- END/top navigation -->


    <!-- page content -->
    <?php
// set page content if logged in user is an Admin 
if ($this->ion_auth->user()->row()->user_type == 'admin') {
    $this->load->view("templates/_parts/_dashboard/admin/page_content_admin.php");
}
// set page content if logged in user is a client
if ($this->ion_auth->user()->row()->user_type == 'user') {
    $this->load->view("templates/_parts/_dashboard/client/page_content_client.php");
}

?>
    <!-- END/page content -->


    <?php
// prepare the footer

$this->load->view("templates/_parts/_dashboard/dashboard_footer.php"); // load footer