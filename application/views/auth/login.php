<?php

/**------------------------------------------------------------------------------------------------------------------------------------------------
 * @@Name: login
 
 * @@Author: Kyei Amos Mensah <'buyitgh@gmail.com'>
 
 * @Date:         2018-08-22 11:54:30
 * @Last Modified by:   Kyei Amos Mensah
 * @Last Modified time: 2018-08-23 16:11:46

 * @Copyright:      MobileContent.Com Ltd <'owner'>
 
 * @Website:      https://mobilecontent.com.gh
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */

  $this->load->view('templates/_parts/_login/welcome_login_header.php'); // load page header
?>
    
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <?php echo $message; ?>
            <?php echo form_open("auth/login"); ?>
              <h1><i class="fa fa-google-wallet"></i> <?php echo lang('app_name_title'); ?></h1>
              <div>
                <input type="text" class="form-control" name="identity" id="identity" placeholder="Example@yourmail.com" required="" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" required="" />
              </div>
              <div>
                <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-lock"></i>&nbsp; <?php echo lang('login_submit_btn'); ?></button>
                <a class="reset_pass" href="#"><?php echo lang('login_forgot_password'); ?></a>
              </div>
            <?php echo form_close(); ?>
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Don't have account?
                  <a href="#signup" class="to_register"><?php echo nbs(15); ?><i class="fa fa-user"></i>&nbsp; Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <p><?php echo lang('site_copyright_footer'); ?></p>
                </div>
              </div>
            
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <?php echo form_open("auth/create_user"); ?>
              <h1><?php echo lang('create_account_title'); ?></h1>
              <div>
                <input type="text" name="customer_name" class="form-control" placeholder="Name" required="" />
              </div>
              <div>
                <input type="email" name="identity" class="form-control" placeholder="Customer Email" required="" />
              </div>
              <div>
                <input type="text" name="phone" class="form-control" placeholder="Phone Number" required="" />
              </div>
              <div>
                <input type="text" name="ussd_shortcode" class="form-control" placeholder="Shortcode" required="" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <input type="password" name="password_confirm" class="form-control" placeholder="Confirm Password" required="" />
              </div>
              <div>
                <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-send"></i>&nbsp; <?php echo lang('create_account_button'); ?>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link"><?php echo lang('already_a_member_check'); ?>
                  <a href="#signin" class="to_register"><i class="fa fa-key"></i>&nbsp;<?php echo lang('login_submit_btn'); ?></a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <p><?php echo lang('site_copyright_footer'); ?></p>
                </div>
              </div>
            <?php echo form_close(); ?>
          </section>
        </div>
      </div>
    </div>
  </body>

</html>

