    <?php

        /**------------------------------------------------------------------------------------------------------------------------------------------------
         * @@Name: dashboard_footer
         
         * @@Author: Kyei Amos Mensah <'buyitgh@gmail.com'>
         
         * @Date:   2018-08-08 12:39:08
         * @Last Modified by:   Kyei Amos Mensah
         * @Last Modified time: 2018-08-14 21:31:03

         * @Copyright: Eitsec Ghana
         
         * @Website: https://eitsec.com.gh
         *---------------------------------------------------------------------------------------------------------------------------------------------------
         */
        ?>

         <!-- footer content -->
                <footer>
                  <div class="pull-right">
                    <?php echo lang('site_copyright_footer'); ?><a href="https://etisec.com.gh/"> Etisec Ghana</a>
                  </div>
                  <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
              </div>
            </div>

            <!-- jQuery -->
            <script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap -->
            <script src="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- FastClick -->
            <script src="<?php echo base_url(); ?>assets/vendors/fastclick/lib/fastclick.js"></script>
            <!-- NProgress -->
            <script src="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.js"></script>
            <!-- Chart.js -->
            <script src="<?php echo base_url(); ?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
            <!-- jQuery Sparklines -->
            <script src="<?php echo base_url(); ?>assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
            <!-- Flot -->
            <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.pie.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.time.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.stack.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.resize.js"></script>
            <!-- Flot plugins -->
            <script src="<?php echo base_url(); ?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
            <!-- DateJS -->
            <script src="<?php echo base_url(); ?>assets/vendors/DateJS/build/date.js"></script>
            <!-- bootstrap-daterangepicker -->
            <script src="<?php echo base_url(); ?>assets/vendors/moment/min/moment.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
            <!-- PNotify -->
            <script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.buttons.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.nonblock.js"></script>
            
            <!-- Custom Theme Scripts -->
            <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>
        <!-- Google Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-23581568-13', 'auto');
            ga('send', 'pageview');
        </script>
    </body>

</html>