<?php

/**------------------------------------------------------------------------------------------------------------------------------------------------
 * @@Name: dashboard_left_sidebar_student
 
 * @@Author: Kyei Amos Mensah <'buyitgh@gmail.com'>
 
 * @Date:   2018-08-08 13:37:17
 * @Last Modified by:   Kyei Amos Mensah
 * @Last Modified time: 2018-08-19 09:18:49

 * @Copyright: Eitsec Ghana
 
 * @Website: https://eitsec.com.gh
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */

?>

<div class="col-md-3 left_col"> 
          <div class="left_col scroll-view">
            <!-- prepare the menu sidebar here based on user roles -->
            <div class="navbar nav_title" style="border: 0;">
              <a href="index-2.html" class="site_title"><i class="fa fa-paw"></i> <span><?php echo lang('app_name_title'); ?></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="assets/img/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span><b>Client Email:</b></span>
                <h2><strong><?php echo $this->ion_auth->user()->row()->email; ?></strong></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3>General</h3> -->
                <ul class="nav side-menu">
                  <li><a href="<?php echo base_url('auth'); ?>"><i class="fa fa-home"></i> Dashboard</a>
                    
                  </li>

                  <!-- user adding new code ui.............. -->
                  <li>
                    <a><i class="fa fa-edit"></i> Ussd Code Ext <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url('auth/'); ?>">Extend Ussd Code</a></li>
                      <li><a href="<?php echo base_url('auth/'); ?>">Manage Extension</a></li>
                    </ul>
                  </li>

                  <li><a href="<?php echo base_url('candidates'); ?>"><i class="fa fa-eye"></i> View Candidates </a>
                  </li>
                  <li><a><i class="fa fa-line-chart"></i> Results </a>
                    <ul class="nav child_menu">
                      <!-- <li><a href="general_elements.html">General Elements</a></li>
                      <li><a href="media_gallery.html">Media Gallery</a></li> -->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bell-o"></i> Notifications</a>
                  </li>
                  <li><a href="<?php echo base_url('auth/logout'); ?>"><i class="fa fa-power-off"></i> Logout</a>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">

              <!-- <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true">sd</span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true">ssd</span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true">fds</span>
              </a> -->

              <!-- <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url('auth/logout'); ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a> -->
            <div class="pull-right">Debug: page loaded in: {elapsed_time}sec</div>
            </div>
            <!-- /menu footer buttons -->
          </div>
          <!-- End Left Menu -->
        </div>