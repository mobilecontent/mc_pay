<?php

/**------------------------------------------------------------------------------------------------------------------------------------------------
 * @@Name: page_content_student
 
 * @@Author: Kyei Amos Mensah <'buyitgh@gmail.com'>
 
 * @Date:   2018-08-09 03:53:37
 * @Last Modified by:   Kyei Amos Mensah
 * @Last Modified time: 2018-08-19 08:55:22

 * @Copyright: Eitsec Ghana
 
 * @Website: https://eitsec.com.gh
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */
if (!defined('BASEPATH')) exit('What do you think you\'re *DOING* boy..?  be Careful not to get the Village Elders *ANGRY* !!!'); //prevent direct Access.!
?>

<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa fa-thumbs-o-up"></i></div>
          <div class="count"><?php echo number_format(2000); ?></div>
          <h3>Voters</h3>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-university"></i></div>
          <div class="count"><?php echo number_format(200); ?></div>
          <h3>Halls</h3>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-users"></i></div>
          <div class="count"><?php echo number_format(18); ?></div>
          <h3>Candidates</h3>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-check-square-o"></i></div>
          <div class="count"><?php echo number_format(50); ?></div>
          <h3>Security Panels</h3>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Vote Summary <small>In progress</small></h2>
            <!-- Date dropdown -->
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="col-md-9 col-sm-12 col-xs-12">
              <div class="demo-container" style="height:280px">
                <div id="chart_plot_02" class="demo-placeholder"></div>
              </div>
              <div class="tiles">
                <div class="col-md-4 tile">
                  <span>Total Votes Recieved</span>
                  <h2>231,809</h2>
                  <span class="sparkline11 graph" style="height: 160px;">
                   <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                 </span>
               </div>
              <!--  <div class="col-md-4 tile">
                <span>Total Revenue</span>
                <h2>$231,809</h2>
                <span class="sparkline22 graph" style="height: 160px;">
                  <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                </span>
              </div> -->
              <!-- <div class="col-md-4 tile">
                <span>Total Sessions</span>
                <h2>231,809</h2>
                <span class="sparkline11 graph" style="height: 160px;">
                 <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
               </span>
             </div> -->
           </div>
         </div>
         <div class="col-md-3 col-sm-12 col-xs-12">
          <div>
            <div class="x_title">
              <h2>Leader Board</h2>
              <ul class="nav navbar-right panel_toolbox">
              </ul>
              <div class="clearfix"></div>
            </div>
            <ul class="list-unstyled top_profiles scroll-view">
              <?php foreach ($get_candidates_records as $candidate_records):?>   <!-- Loop through records -->
              <li class="media event">
                <a class="pull-left border-green profile_thumb">
                  <i class="fa fa-user aero"></i>
                </a>
                <div class="media-body">
                  <a class="title" href="#"><?php echo $candidate_records->candidate_name; ?></a>
                  <p><strong><?php echo number_format($candidate_records->total_votes); ?> votes</strong> </p>
                  <small><b>Hall:</b> CommonWealth</small>
                  <p><small><b>Position: </b><?php echo $candidate_records->contesting_position; ?></small>
                  </p>
                </div>
              </li>
              <?php endforeach ?> <!-- End foreach statment -->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
</div>
</div>