
    <!-- new ussd creation form -->
            <div class="row">
              <div  class="col-md-4  col-md-offset-4"><!-- col-md-12 col-sm-12 col-xs-12 -->
                <div class="x_panel">

                  <div class="x_title">
                    
                     <!-- prepare the menu sidebar here based on user roles -->
                    <div class="navbar nav_title" style="border: 0;">
                      <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>
                        <?php echo lang('app_name_title'); ?>
                          
                        </span></a>
                    </div>

                    <div class="clearfix"></div>
                  </div>
                  <h3 class="text-center">Ussd Extention Creation </h3>
                  <div class="x_content">
                    <small>Enter your ussd code and the Extension you wish to create</small>
                    <br>
                    <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
              
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fullname">
                          Full Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="fullname" required="required" class="form-control col-md-6 col-xs-12" type="text" name="UserFullName" placeholder="Full Name" autofocus="true">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">
                          Contact <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="phone" name="phone" required="required" class="form-control col-md-6 col-xs-12" type="text" placeholder="Phone Number">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="ussd" class="control-label col-md-3 col-sm-3 col-xs-12">
                          USSD Code<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                            <input type="text" id="ussd_code" class="form-control" name="ussd_code" placeholder="ussd">
                          </div>
                           
                          <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                            <input type="text" placeholder="extention" class="form-control" name="new_ussd_extension" id="new_extension">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success"><b>Create</b></button>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <button class="btn btn-danger" type="reset"><b>Cancel</b></button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>    



