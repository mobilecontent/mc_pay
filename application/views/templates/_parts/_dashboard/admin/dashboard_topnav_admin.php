<?php

/**------------------------------------------------------------------------------------------------------------------------------------------------
 * @@Name: dashboard_topnav_admin
 
 * @@Author: Kyei Amos Mensah <'buyitgh@gmail.com'>
 
 * @Date:   2018-08-08 15:43:06
 * @Last Modified by:   Kyei Amos Mensah
 * @Last Modified time: 2018-08-08 15:43:22

 * @Copyright: Eitsec Ghana
 
 * @Website: https://eitsec.com.gh
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */

echo "Admin NavBar";