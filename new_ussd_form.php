
    <!-- new ussd creation form -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                  <div class="x_title">
                    <h2 class="text-center">New Ussd Form <small>Enter your and the Extension you wish to create</small></h2>

                     <!-- prepare the menu sidebar here based on user roles -->
                    <div class="navbar nav_title" style="border: 0;">
                      <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>
                        <?php echo lang('app_name_title'); ?>
                          
                        </span></a>
                    </div>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
              
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Full Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="fullname" required="required" class="form-control col-md-6 col-xs-12" type="text" name="UseFullName">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Contact <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="phone" name="phone" required="required" class="form-control col-md-6 col-xs-12" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="ussd" class="control-label col-md-3 col-sm-3 col-xs-12">Your USSD Code<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          

                          <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                            <input type="text" d="ussd_code" class="form-control" name="ussd_code">
                          </div>
                            &nbsp;&nbsp;&nbsp; &nbsp;
                          <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                            <input type="text" placeholder="Enter new extension you wish to create" class="form-control" name="new_ussd_extension" id="new_extension">
                          </div>
                        </div>
                      </div>
              
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Submit</button>
                          <button class="btn btn-primary" type="button">Cancel</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>  



